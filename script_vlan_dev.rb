require './defs_ips'

email =ARGV[0]
ip = ARGV[1]

if ARGV.length == 0
    @logger_stdout.info "Argumentos não informados."
elsif ARGV.length == 1
    get_ip(email)
else
    release_ip(email, ip)
end
