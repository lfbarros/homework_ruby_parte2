require 'logger'

@logger_log = Logger.new("vlan_dev.log")
@logger_stdout = Logger.new(STDOUT)


def get_ip(user)
    vlan_dev = File.open("vlan_dev.txt").read
    vlan_dev_array = vlan_dev.split("\n")
    if File.zero? ("vlan_dev.txt")
        @logger_stdout.info "Prezado #{user},"
        @logger_stdout.info "Não existem mais endereços disponiveis na lista de ip, estão todos em uso neste momento."
    else
        @logger_log.info "O ip #{vlan_dev_array[0]} foi reservado por #{user} para o seu uso."
        @logger_log.info "Favor executar o script informando o ip ao final do uso, para que o mesmo retorne para lista e possa ser reutilizado."
        vlan_dev_array.delete(vlan_dev_array[0])
        new_array = File.open("vlan_dev.txt", 'w')
        new_array.puts vlan_dev_array
        new_array.close
    end
end

def release_ip(user, ip)
    vlan_dev = File.open("vlan_dev.txt").read
    vlan_dev_array = vlan_dev.split("\n")
    if vlan_dev_array.include?(ip)
        @logger_stdout.info "Prezado #{user},"
        @logger_stdout.info "O endereco ip #{ip} ja se encontra na lista de IP liberados."
    else
        @logger_log.info "O ip #{ip} foi liberado para uso, por #{user}."
        new_array = File.open("vlan_dev.txt", 'a')
        new_array.puts ip
        new_array.close
    end
end
